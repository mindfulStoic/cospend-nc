# Nextcloud Cospend 💰

Nextcloud Cospend est un gestionnaire de dépenses partagées (de groupe). Il a été inspiré par le génial [IHateMoney](https://github.com/spiral-project/ihatemoney/).

Vous pouvez l'utiliser lorsque vous partagez une maison, quand vous partez en vacances avec des amis, chaque fois que vous partagez des dépenses avec autrui.

Cospend vous permet de créer des projets avec des membres et des factures. Chaque membre a un solde calculé à partir des factures du projet. Comme ça vous pouvez voir qui doit de l'argent au groupe et à qui le groupe doit de l'argent. À la fin, vous pouvez demander un plan de remboursement qui vous indique les paiements à effectuer pour remettre les soldes des membres à zéro.

Les membres du projets sont indépendants des utilisateurs Nextcloud. Une fois que vous avez accès à un projet (en tant qu'invité ou en tant qu'utilisateur Nextcloud), il n'y a aucune restriction sur ce que vous pouvez ajouter/éditer/supprimer. Les projets peuvent être consultés et modifiés par des personnes sans compte Nextcloud. Chaque projet a un identifiant et un mot de passe pour l'accès invité.

Le client Android [MoneyBuster](https://gitlab.com/eneiluj/moneybuster) est [disponible dans F-Droid](https://f-droid.org/packages/net.eneiluj.moneybuster/).

## Fonctionnalités

* ✎ créer/éditer/supprimer des projets, membres, factures
* ⚖ voir les soldes des membres
* 🗠 afficher les statistiques des projets
* ♻ afficher un plan de remboursement
* 🎇 créer automatiquement les factures correspondant au plan de remboursement
* 🗓 créer des factures récurrentes (jour/semaine/mois/année)
* 📊 entrer un montant personnalisé pour chaque membre dans les nouvelles factures
* 🔗 insérer un lien public vers un fichier personnel dans la description de la facture (photo de la facture physique par exemple)
* 👩 accès invité pour les personnes en dehors du Nextcloud
* 👫 partager un projet avec des utilisateurs Nextcloud
* 🖫 importer/exporter des projets en csv (compatible avec les fichiers csv d'IHateMoney)
* 🖧 ajouter des projets externes (hébergés par une autre instance Nextcloud)
* 🔗 générez des liens/QRCode pour facilement importer des projets dans MoneyBuster

Cette application est testée sur Nextcloud 15 avec Firefox 57+ et Chromium.

Cette appli est en développement.

🌍 Aidez-nous à traduire cette application sur [Nextcloud-Cospend/MoneyBuster Crowdin projet](https://crowdin.com/project/moneybuster).

⚒ Découvrez d'autres façons d'aider dans les [indications de contribution](https://gitlab.com/eneiluj/cospend-nc/blob/master/CONTRIBUTING.md).

## Installer

Voir l'[AdminDoc](https://gitlab.com/eneiluj/cospend-nc/wikis/admindoc) pour les détails de l'installation.

Faites un tour vers le fichier [CHANGELOG](https://gitlab.com/eneiluj/cospend-nc/blob/master/CHANGELOG.md#change-log) pour voir ce qui est nouveau et ce qui arrive dans la prochaine version.

Lisez le fichier [AUTHORS](https://gitlab.com/eneiluj/cospend-nc/blob/master/AUTHORS.md#authors) pour voir la liste complète des auteurs.

## Problèmes connus

* ça ne vous rend pas riche

Tout retour sera apprécié.