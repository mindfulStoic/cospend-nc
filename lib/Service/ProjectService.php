<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Service;

use OCP\IL10N;
use OCP\ILogger;
use OCP\DB\QueryBuilder\IQueryBuilder;

use OC\Archive\ZIP;
use OCP\IGroupManager;
use OCP\IAvatarManager;

use OCP\IUserManager;
use OCP\Share\IManager;
use OCP\IServerContainer;

use OCA\Cospend\Activity\ActivityManager;
use OCA\Cospend\Db\ProjectMapper;
use OCA\Cospend\Db\BillMapper;

class ProjectService {

    private $l10n;
    private $logger;
    private $qb;
    private $dbconnection;

    public function __construct (ILogger $logger,
                                IL10N $l10n,
                                ProjectMapper $projectMapper,
                                BillMapper $billMapper,
                                ActivityManager $activityManager,
                                IAvatarManager $avatarManager,
                                IManager $shareManager,
                                IUserManager $userManager,
                                IGroupManager $groupManager) {
        $this->trans = $l10n;
        $this->logger = $logger;
        $this->qb = \OC::$server->getDatabaseConnection()->getQueryBuilder();
        $this->dbconnection = \OC::$server->getDatabaseConnection();
        $this->projectMapper = $projectMapper;
        $this->billMapper = $billMapper;
        $this->activityManager = $activityManager;
        $this->avatarManager = $avatarManager;
        $this->groupManager = $groupManager;
        $this->userManager = $userManager;
        $this->shareManager = $shareManager;
    }

    private function db_quote_escape_string($str){
        return $this->dbconnection->quote($str);
    }

    /**
     * check if user owns the project
     * or if the project is shared with the user
     */
    public function userCanAccessProject($userid, $projectid) {
        $projectInfo = $this->getProjectInfo($projectid);
        if ($projectInfo !== null) {
            // does the user own the project ?
            if ($projectInfo['userid'] === $userid) {
                return true;
            }
            else {
                $qb = $this->dbconnection->getQueryBuilder();
                // is the project shared with the user ?
                $qb->select('userid', 'projectid')
                    ->from('cospend_shares', 's')
                    ->where(
                        $qb->expr()->eq('isgroupshare', $qb->createNamedParameter(0, IQueryBuilder::PARAM_INT))
                    )
                    ->andWhere(
                        $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
                    )
                    ->andWhere(
                        $qb->expr()->eq('userid', $qb->createNamedParameter($userid, IQueryBuilder::PARAM_STR))
                    );
                $req = $qb->execute();
                $dbProjectId = null;
                while ($row = $req->fetch()) {
                    $dbProjectId = $row['projectid'];
                    break;
                }
                $req->closeCursor();
                $qb = $qb->resetQueryParts();

                if ($dbProjectId !== null) {
                    return true;
                }
                else {
                    // if not, is the project shared with a group containing the user?
                    $userO = $this->userManager->get($userid);

                    $qb->select('userid')
                        ->from('cospend_shares', 's')
                        ->where(
                            $qb->expr()->eq('isgroupshare', $qb->createNamedParameter(1, IQueryBuilder::PARAM_INT))
                        )
                        ->andWhere(
                            $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
                        );
                    $req = $qb->execute();
                    while ($row = $req->fetch()){
                        $groupId = $row['userid'];
                        if ($this->groupManager->groupExists($groupId) && $this->groupManager->get($groupId)->inGroup($userO)) {
                            return true;
                        }
                    }
                    $req->closeCursor();
                    $qb = $qb->resetQueryParts();

                    return false;
                }
            }
        }
        else {
            return false;
        }
    }

    /**
     * check if user owns the external project
     */
    public function userCanAccessExternalProject($userid, $projectid, $ncurl) {
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('projectid')
           ->from('cospend_ext_projects', 'ep')
           ->where(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
           )
           ->andWhere(
               $qb->expr()->eq('ncurl', $qb->createNamedParameter($ncurl, IQueryBuilder::PARAM_STR))
           )
           ->andWhere(
               $qb->expr()->eq('userid', $qb->createNamedParameter($userid, IQueryBuilder::PARAM_STR))
           );
        $req = $qb->execute();
        $dbProjectId = null;
        while ($row = $req->fetch()){
            $dbProjectId = $row['projectid'];
            break;
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();

        return ($dbProjectId !== null);
    }

    public function createProject($name, $id, $password, $contact_email, $userid='') {
        $qb = $this->dbconnection->getQueryBuilder();

        $qb->select('id')
           ->from('cospend_projects', 'p')
           ->where(
               $qb->expr()->eq('id', $qb->createNamedParameter($id, IQueryBuilder::PARAM_STR))
           );
        $req = $qb->execute();

        $dbid = null;
        while ($row = $req->fetch()){
            $dbid = $row['id'];
            break;
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();
        if ($dbid === null) {
            // check if id is valid
            if (strpos($id, '/') !== false) {
                return ['message'=>'Invalid project id'];
            }
            $dbPassword = '';
            if ($password !== null && $password !== '') {
                $dbPassword = password_hash($password, PASSWORD_DEFAULT);
            }
            if ($contact_email === null) {
                $contact_email = '';
            }
            $ts = (new \DateTime())->getTimestamp();
            $qb->insert('cospend_projects')
                ->values([
                    'userid' => $qb->createNamedParameter($userid, IQueryBuilder::PARAM_STR),
                    'id' => $qb->createNamedParameter($id, IQueryBuilder::PARAM_STR),
                    'name' => $qb->createNamedParameter($name, IQueryBuilder::PARAM_STR),
                    'password' => $qb->createNamedParameter($dbPassword, IQueryBuilder::PARAM_STR),
                    'email' => $qb->createNamedParameter($contact_email, IQueryBuilder::PARAM_STR),
                    'lastchanged' => $qb->createNamedParameter($ts, IQueryBuilder::PARAM_INT)
                ]);
            $req = $qb->execute();
            $qb = $qb->resetQueryParts();

            return $id;
        }
        else {
            return ['message'=>'A project with id "'.$id.'" already exists'];
        }
    }

    public function addExternalProject($ncurl, $id, $password, $userid) {
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('projectid')
           ->from('cospend_ext_projects', 'ep')
           ->where(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($id, IQueryBuilder::PARAM_STR))
           )
           ->andWhere(
               $qb->expr()->eq('ncurl', $qb->createNamedParameter($ncurl, IQueryBuilder::PARAM_STR))
           );
        $req = $qb->execute();
        $dbprojectid = null;
        while ($row = $req->fetch()){
            $dbprojectid = $row['projectid'];
            break;
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();
        if ($dbprojectid === null) {
            // check if id is valid
            if (strpos($id, '/') !== false) {
                return ['message'=>'Invalid project id'];
            }
            $qb->insert('cospend_ext_projects')
                ->values([
                    'userid' => $qb->createNamedParameter($userid, IQueryBuilder::PARAM_STR),
                    'projectid' => $qb->createNamedParameter($id, IQueryBuilder::PARAM_STR),
                    'ncurl' => $qb->createNamedParameter($ncurl, IQueryBuilder::PARAM_STR),
                    'password' => $qb->createNamedParameter($password, IQueryBuilder::PARAM_STR)
                ]);
            $req = $qb->execute();

            return $id;
        }
        else {
            return ['message'=>'A project with id "'.$id.'" and url "'.$ncurl.'" already exists'];
        }
    }

    public function deleteProject($projectid) {
        $projectToDelete = $this->getProjectById($projectid);
        if ($projectToDelete !== null) {
            $qb = $this->dbconnection->getQueryBuilder();

            // delete project bills
            $bills = $this->getBills($projectid);
            foreach ($bills as $bill) {
                $this->deleteBillOwersOfBill($bill['id']);
            }

            $qb->delete('cospend_bills')
                ->where(
                    $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
                );
            $req = $qb->execute();
            $qb = $qb->resetQueryParts();

            // delete project members
            $qb->delete('cospend_members')
                ->where(
                    $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
                );
            $req = $qb->execute();
            $qb = $qb->resetQueryParts();

            // delete shares
            $qb->delete('cospend_shares')
                ->where(
                    $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
                );
            $req = $qb->execute();
            $qb = $qb->resetQueryParts();

            // delete project
            $qb->delete('cospend_projects')
                ->where(
                    $qb->expr()->eq('id', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
                );
            $req = $qb->execute();
            $qb = $qb->resetQueryParts();

            return 'DELETED';
        }
        else {
            return ['Not Found'];
        }
    }

    public function getProjectInfo($projectid) {
        $projectInfo = null;

        $qb = $this->dbconnection->getQueryBuilder();

        $qb->select('id', 'password', 'name', 'email', 'userid', 'lastchanged')
           ->from('cospend_projects', 'p')
           ->where(
               $qb->expr()->eq('id', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
           );
        $req = $qb->execute();

        $dbProjectId = null;
        $dbPassword = null;
        while ($row = $req->fetch()){
            $dbProjectId = $row['id'];
            $dbPassword = $row['password'];
            $dbName = $row['name'];
            $dbEmail= $row['email'];
            $dbUserId = $row['userid'];
            $dbLastchanged = intval($row['lastchanged']);
            break;
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();
        if ($dbProjectId !== null) {
            $members = $this->getMembers($dbProjectId);
            $activeMembers = [];
            foreach ($members as $member) {
                if ($member['activated']) {
                    array_push($activeMembers, $member);
                }
            }
            $balance = $this->getBalance($dbProjectId);
            $projectInfo = [
                'userid'=>$dbUserId,
                'name'=>$dbName,
                'contact_email'=>$dbEmail,
                'id'=>$dbProjectId,
                'active_members'=>$activeMembers,
                'members'=>$members,
                'balance'=>$balance,
                'lastchanged'=>$dbLastchanged
            ];
        }

        return $projectInfo;
    }

    public function getProjectStatistics($projectId, $memberOrder=null, $dateMin=null, $dateMax=null,
                                          $paymentMode=null, $category=null, $amountMin=null, $amountMax=null) {
        $membersWeight = [];
        $membersNbBills = [];
        $membersBalance = [];
        $membersPaid = [];
        $membersSpent = [];

        $members = $this->getMembers($projectId, $memberOrder);
        foreach ($members as $member) {
            $memberId = $member['id'];
            $memberWeight = $member['weight'];
            $membersWeight[$memberId] = $memberWeight;
            $membersNbBills[$memberId] = 0;
            $membersBalance[$memberId] = 0.0;
            $membersPaid[$memberId] = 0.0;
            $membersSpent[$memberId] = 0.0;
        }

        $bills = $this->getBills($projectId, $dateMin, $dateMax, $paymentMode, $category, $amountMin, $amountMax);
        foreach ($bills as $bill) {
            $payerId = $bill['payer_id'];
            $amount = $bill['amount'];
            $owers = $bill['owers'];

            $membersNbBills[$payerId]++;
            $membersBalance[$payerId] += $amount;
            $membersPaid[$payerId] += $amount;

            $nbOwerShares = 0.0;
            foreach ($owers as $ower) {
                $owerWeight = $ower['weight'];
                if ($owerWeight === 0.0) {
                    $owerWeight = 1.0;
                }
                $nbOwerShares += $owerWeight;
            }
            foreach ($owers as $ower) {
                $owerWeight = $ower['weight'];
                if ($owerWeight === 0.0) {
                    $owerWeight = 1.0;
                }
                $owerId = $ower['id'];
                $spent = $amount / $nbOwerShares * $owerWeight;
                $membersBalance[$owerId] -= $spent;
                $membersSpent[$owerId] += $spent;
            }
        }

        $statistics = [];
        foreach ($members as $member) {
            $memberId = $member['id'];
            $statistic = [
                'balance' => $membersBalance[$memberId],
                'paid' => $membersPaid[$memberId],
                'spent' => $membersSpent[$memberId],
                'member' => $member
            ];
            array_push($statistics, $statistic);
        }

        return $statistics;
    }

    public function addBill($projectid, $date, $what, $payer, $payed_for, $amount, $repeat, $paymentmode=null, $categoryid=null) {
        if ($repeat === null || $repeat === '' || strlen($repeat) !== 1) {
            return ['repeat'=> ['Invalid value.']];
        }
        if ($date === null || $date === '') {
            return ['date'=> ['This field is required.']];
        }
        if ($what === null || $what === '') {
            return ['what'=> ['This field is required.']];
        }
        if ($amount === null || $amount === '' || !is_numeric($amount)) {
            return ['amount'=> ['This field is required.']];
        }
        if ($payer === null || $payer === '' || !is_numeric($payer)) {
            return ['payer'=> ['This field is required.']];
        }
        if ($this->getMemberById($projectid, $payer) === null) {
            return ['payer'=>['Not a valid choice']];
        }
        // check owers
        $owerIds = explode(',', $payed_for);
        if ($payed_for === null || $payed_for === '' || count($owerIds) === 0) {
            return ['payed_for'=>['Invalid value']];
        }
        foreach ($owerIds as $owerId) {
            if (!is_numeric($owerId)) {
                return ['payed_for'=>['Invalid value']];
            }
            if ($this->getMemberById($projectid, $owerId) === null) {
                return ['payed_for'=>['Not a valid choice']];
            }
        }

        // last modification timestamp is now
        $ts = (new \DateTime())->getTimestamp();

        // do it already !
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->insert('cospend_bills')
            ->values([
                'projectid' => $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR),
                'what' => $qb->createNamedParameter($what, IQueryBuilder::PARAM_STR),
                'date' => $qb->createNamedParameter($date, IQueryBuilder::PARAM_STR),
                'amount' => $qb->createNamedParameter($amount, IQueryBuilder::PARAM_STR),
                'payerid' => $qb->createNamedParameter($payer, IQueryBuilder::PARAM_INT),
                'repeat' => $qb->createNamedParameter($repeat, IQueryBuilder::PARAM_STR),
                'categoryid' => $qb->createNamedParameter($categoryid, IQueryBuilder::PARAM_INT),
                'paymentmode' => $qb->createNamedParameter($paymentmode, IQueryBuilder::PARAM_STR),
                'lastchanged' => $qb->createNamedParameter($ts, IQueryBuilder::PARAM_INT)
            ]);
        $req = $qb->execute();
        $qb = $qb->resetQueryParts();

        $insertedBillId = $qb->getLastInsertId();

        // insert bill owers
        foreach ($owerIds as $owerId) {
            $qb->insert('cospend_bill_owers')
                ->values([
                    'billid' => $qb->createNamedParameter($insertedBillId, IQueryBuilder::PARAM_INT),
                    'memberid' => $qb->createNamedParameter($owerId, IQueryBuilder::PARAM_INT)
                ]);
            $req = $qb->execute();
            $qb = $qb->resetQueryParts();
        }

        return $insertedBillId;
    }

    public function deleteBill($projectid, $billid) {
        $billToDelete = $this->getBill($projectid, $billid);
        if ($billToDelete !== null) {
            $this->deleteBillOwersOfBill($billid);

            $qb = $this->dbconnection->getQueryBuilder();
            $qb->delete('cospend_bills')
               ->where(
                   $qb->expr()->eq('id', $qb->createNamedParameter($billid, IQueryBuilder::PARAM_INT))
               )
               ->andWhere(
                   $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
               );
            $req = $qb->execute();
            $qb = $qb->resetQueryParts();

            return 'OK';
        }
        else {
            return ["message" => "Not Found"];
        }
    }

    private function getMemberById($projectId, $memberId) {
        $member = null;

        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('id', 'name', 'weight', 'activated')
           ->from('cospend_members', 'm')
           ->where(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
           )
           ->andWhere(
               $qb->expr()->eq('id', $qb->createNamedParameter($memberId, IQueryBuilder::PARAM_INT))
           );
        $req = $qb->execute();

        while ($row = $req->fetch()){
            $dbMemberId = intval($row['id']);
            $dbWeight = floatval($row['weight']);
            $dbName = $row['name'];
            $dbActivated= intval($row['activated']);
            $member = [
                    'activated' => ($dbActivated === 1),
                    'name' => $dbName,
                    'id' => $dbMemberId,
                    'weight' => $dbWeight
            ];
            break;
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();
        return $member;
    }

    private function getProjectById($projectId) {
        $project = null;

        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('id', 'userid', 'name', 'email', 'password')
           ->from('cospend_projects', 'p')
           ->where(
               $qb->expr()->eq('id', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
           );
        $req = $qb->execute();

        while ($row = $req->fetch()){
            $dbId = $row['id'];
            $dbPassword = $row['password'];
            $dbName = $row['name'];
            $dbUserId = $row['userid'];
            $dbEmail = $row['email'];
            $project = [
                    'id' => $dbId,
                    'name' => $dbName,
                    'userid' => $dbUserId,
                    'password' => $dbPassword,
                    'email' => $dbEmail
            ];
            break;
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();
        return $project;
    }

    public function getBill($projectId, $billId) {
        $bill = null;
        // get bill owers
        $billOwers = [];

        $qb = $this->dbconnection->getQueryBuilder();

        $qb->select('memberid', 'm.name', 'm.weight', 'm.activated')
           ->from('cospend_bill_owers', 'bo')
           ->innerJoin('bo', 'cospend_members', 'm', $qb->expr()->eq('bo.memberid', 'm.id'))
           ->where(
               $qb->expr()->eq('bo.billid', $qb->createNamedParameter($billId, IQueryBuilder::PARAM_INT))
           );
        $req = $qb->execute();

        while ($row = $req->fetch()){
            $dbWeight = floatval($row['weight']);
            $dbName = $row['name'];
            $dbActivated = (intval($row['activated']) === 1);
            $dbOwerId= intval($row['memberid']);
            array_push(
                $billOwers,
                [
                    'id' => $dbOwerId,
                    'weight' => $dbWeight,
                    'name' => $dbName,
                    'activated' => $dbActivated
                ]
            );
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();

        // get the bill
        $qb->select('id', 'what', 'date', 'amount', 'payerid', 'repeat', 'paymentmode', 'categoryid')
           ->from('cospend_bills', 'b')
           ->where(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
           )
           ->andWhere(
               $qb->expr()->eq('id', $qb->createNamedParameter($billId, IQueryBuilder::PARAM_INT))
           );
        $req = $qb->execute();
        while ($row = $req->fetch()){
            $dbBillId = intval($row['id']);
            $dbAmount = floatval($row['amount']);
            $dbWhat = $row['what'];
            $dbDate = $row['date'];
            $dbRepeat = $row['repeat'];
            $dbPayerId = intval($row['payerid']);
            $dbPaymentMode = $row['paymentmode'];
            $dbCategoryId = intval($row['categoryid']);
            $bill = [
                'id' => $dbBillId,
                'amount' => $dbAmount,
                'what' => $dbWhat,
                'date' => $dbDate,
                'payer_id' => $dbPayerId,
                'owers' => $billOwers,
                'repeat' => $dbRepeat,
                'paymentmode' => $dbPaymentMode,
                'categoryid' => $dbCategoryId
            ];
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();

        return $bill;
    }

    private function deleteBillOwersOfBill($billid) {
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->delete('cospend_bill_owers')
           ->where(
               $qb->expr()->eq('billid', $qb->createNamedParameter($billid, IQueryBuilder::PARAM_INT))
           );
        $req = $qb->execute();
        $qb = $qb->resetQueryParts();
    }

    public function autoSettlement($projectid) {
        $transactions = $this->getProjectSettlement($projectid);
        if (!is_array($transactions)) {
            return ['message'=>'Error when getting project settlement transactions'];
        }

        $members = $this->getMembers($projectid);
        $memberIdToName = [];
        foreach ($members as $member) {
            $memberIdToName[$member['id']] = $member['name'];
        }

        $now = new \DateTime();
        $date = $now->format('Y-m-d');

        foreach ($transactions as $transaction) {
            $fromId = $transaction['from'];
            $toId = $transaction['to'];
            $amount = floatval($transaction['amount']);
            $billTitle = $memberIdToName[$fromId].' → '.$memberIdToName[$toId];
            $addBillResult = $this->addBill($projectid, $date, $billTitle, $fromId, $toId, $amount, 'n');
            if (!is_numeric($addBillResult)) {
                return ['message'=>'Error when addind a bill'];
            }
        }
        return 'OK';
    }

    public function getProjectSettlement($projectId) {
        $balances = $this->getBalance($projectId);
        $transactions = $this->settle($balances);
        return $transactions;
    }

    private function settle($balances) {
        $debitersCrediters = $this->orderBalance($balances);
        $debiters = $debitersCrediters[0];
        $crediters = $debitersCrediters[1];
        return $this->reduceBalance($crediters, $debiters);
    }

    private function orderBalance($balances) {
        $crediters = [];
        $debiters = [];
        foreach ($balances as $id => $balance) {
            if ($balance > 0.0) {
                array_push($crediters, [$id, $balance]);
            }
            else if ($balance < 0.0) {
                array_push($debiters, [$id, $balance]);
            }
        }

        return [$debiters, $crediters];
    }

    private function reduceBalance($crediters, $debiters, $results=null) {
        if (count($crediters) === 0 or count($debiters) === 0) {
            return $results;
        }

        if ($results === null) {
            $results = [];
        }

        $crediters = $this->sortCreditersDebiters($crediters);
        $debiters = $this->sortCreditersDebiters($debiters, true);

        $deb = array_pop($debiters);
        $debiter = $deb[0];
        $debiterBalance = $deb[1];

        $cred = array_pop($crediters);
        $crediter = $cred[0];
        $crediterBalance = $cred[1];

        if (abs($debiterBalance) > abs($crediterBalance)) {
            $amount = abs($crediterBalance);
        }
        else {
            $amount = abs($debiterBalance);
        }

        $newResults = $results;
        array_push($newResults, ['to'=>$crediter, 'amount'=>$amount, 'from'=>$debiter]);

        $newDebiterBalance = $debiterBalance + $amount;
        if ($newDebiterBalance < 0.0) {
            array_push($debiters, [$debiter, $newDebiterBalance]);
            $debiters = $this->sortCreditersDebiters($debiters, true);
        }

        $newCrediterBalance = $crediterBalance - $amount;
        if ($newCrediterBalance > 0.0) {
            array_push($crediters, [$crediter, $newCrediterBalance]);
            $crediters = $this->sortCreditersDebiters($crediters);
        }

        return $this->reduceBalance($crediters, $debiters, $newResults);
    }

    private function sortCreditersDebiters($arr, $reverse=false) {
        $res = [];
        if ($reverse) {
            foreach ($arr as $elem) {
                $i = 0;
                while ($i < count($res) and $elem[1] < $res[$i][1]) {
                    $i++;
                }
                array_splice($res, $i, 0, [$elem]);
            }
        }
        else {
            foreach ($arr as $elem) {
                $i = 0;
                while ($i < count($res) and $elem[1] >= $res[$i][1]) {
                    $i++;
                }
                array_splice($res, $i, 0, [$elem]);
            }
        }
        return $res;
    }

    public function editMember($projectid, $memberid, $name, $weight, $activated) {
        if ($name !== null && $name !== '') {
            if ($this->getMemberById($projectid, $memberid) !== null) {
                $qb = $this->dbconnection->getQueryBuilder();
                $qb->update('cospend_members');
                if ($weight !== null && $weight !== '') {
                    if (is_numeric($weight) and floatval($weight) > 0.0) {
                        $newWeight = floatval($weight);
                        $qb->set('weight', $qb->createNamedParameter($newWeight, IQueryBuilder::PARAM_STR));
                    }
                    else {
                        return ['weight' => ['Not a valid decimal value']];
                    }
                }
                if ($activated !== null && $activated !== '' && ($activated === 'true' || $activated === 'false')) {
                    $qb->set('activated', $qb->createNamedParameter(($activated === 'true' ? 1 : 0), IQueryBuilder::PARAM_INT));
                }

                $ts = (new \DateTime())->getTimestamp();
                $qb->set('lastchanged', $qb->createNamedParameter($ts, IQueryBuilder::PARAM_INT));

                $qb->set('name', $qb->createNamedParameter($name, IQueryBuilder::PARAM_STR));
                $qb->where(
                    $qb->expr()->eq('id', $qb->createNamedParameter($memberid, IQueryBuilder::PARAM_INT))
                )
                ->andWhere(
                    $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
                );
                $req = $qb->execute();
                $qb = $qb->resetQueryParts();

                $editedMember = $this->getMemberById($projectid, $memberid);

                $av = $this->avatarManager->getGuestAvatar($editedMember['name']);
                $color = $av->avatarBackgroundColor($editedMember['name']);
                $editedMember['color'] = $color;

                return $editedMember;
            }
            else {
                return ['name'=>['This project have no such member']];
            }
        }
        else {
            return ['name'=> ['This field is required.']];
        }
    }

    public function editProject($projectid, $name, $contact_email, $password, $autoexport=null) {
        if ($name === null || $name === '') {
            return ['name'=> ['This field is required.']];
        }

        $qb = $this->dbconnection->getQueryBuilder();
        $qb->update('cospend_projects');

        $emailSql = '';
        if ($contact_email !== null && $contact_email !== '') {
            if (filter_var($contact_email, FILTER_VALIDATE_EMAIL)) {
                $qb->set('email', $qb->createNamedParameter($contact_email, IQueryBuilder::PARAM_STR));
            }
            else {
                return ['contact_email'=> ['Invalid email address']];
            }
        }
        if ($password !== null && $password !== '') {
            $dbPassword = password_hash($password, PASSWORD_DEFAULT);
            $qb->set('password', $qb->createNamedParameter($dbPassword, IQueryBuilder::PARAM_STR));
        }
        if ($autoexport !== null && $autoexport !== '') {
            $qb->set('autoexport', $qb->createNamedParameter($autoexport, IQueryBuilder::PARAM_STR));
        }
        if ($this->getProjectById($projectid) !== null) {
            $ts = (new \DateTime())->getTimestamp();
            $qb->set('lastchanged', $qb->createNamedParameter($ts, IQueryBuilder::PARAM_INT));
            $qb->set('name', $qb->createNamedParameter($name, IQueryBuilder::PARAM_STR));
            $qb->where(
                $qb->expr()->eq('id', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
            );
            $req = $qb->execute();
            $qb = $qb->resetQueryParts();

            return 'UPDATED';
        }
        else {
            return ['message'=>["There is no such project"]];
        }
    }

    public function editExternalProject($projectid, $ncurl, $password) {
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->update('cospend_ext_projects');
        $qb->set('password', $qb->createNamedParameter($password, IQueryBuilder::PARAM_STR));
        $qb->where(
            $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
        )
        ->andWhere(
            $qb->expr()->eq('ncurl', $qb->createNamedParameter($ncurl, IQueryBuilder::PARAM_STR))
        );
        $req = $qb->execute();
        $qb = $qb->resetQueryParts();

        return 'UPDATED';
    }

    public function deleteExternalProject($projectid, $ncurl) {
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->delete('cospend_ext_projects')
            ->where(
                $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
            )
            ->andWhere(
                $qb->expr()->eq('ncurl', $qb->createNamedParameter($ncurl, IQueryBuilder::PARAM_STR))
            );
        $req = $qb->execute();
        $qb = $qb->resetQueryParts();

        return 'DELETED';
    }

    public function addMember($projectid, $name, $weight) {
        if ($name !== null && $name !== '') {
            if ($this->getMemberByName($projectid, $name) === null) {
                $weightToInsert = 1;
                if ($weight !== null && $weight !== '') {
                    if (is_numeric($weight) and floatval($weight) > 0.0) {
                        $weightToInsert = floatval($weight);
                    }
                    else {
                        return ['weight' => ['Not a valid decimal value']];
                    }
                }

                $ts = (new \DateTime())->getTimestamp();

                $qb = $this->dbconnection->getQueryBuilder();
                $qb->insert('cospend_members')
                    ->values([
                        'projectid' => $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR),
                        'name' => $qb->createNamedParameter($name, IQueryBuilder::PARAM_STR),
                        'weight' => $qb->createNamedParameter($weightToInsert, IQueryBuilder::PARAM_STR),
                        'activated' => $qb->createNamedParameter(1, IQueryBuilder::PARAM_INT),
                        'lastchanged' => $qb->createNamedParameter($ts, IQueryBuilder::PARAM_INT)
                    ]);
                $req = $qb->execute();
                $qb = $qb->resetQueryParts();

                $insertedMember = $this->getMemberByName($projectid, $name);

                return $insertedMember['id'];
            }
            else {
                return ['message'=>['This project already has this member']];
            }
        }
        else {
            return ['name' => ['This field is required.']];
        }
    }

    public function getBills($projectId, $dateMin=null, $dateMax=null, $paymentMode=null, $category=null,
                              $amountMin=null, $amountMax=null, $lastchanged=null) {
        $bills = [];

        // first get all bill ids
        $billIds = [];
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('id')
           ->from('cospend_bills', 'b')
           ->where(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
           );
        // take bills that have changed after $lastchanged
        if ($lastchanged !== null and is_numeric($lastchanged)) {
           $qb->andWhere(
               $qb->expr()->gt('lastchanged', $qb->createNamedParameter(intval($lastchanged), IQueryBuilder::PARAM_INT))
           );
        }
        if ($dateMin !== null and $dateMin !== '') {
           $qb->andWhere(
               $qb->expr()->gte('date', $qb->createNamedParameter($dateMin, IQueryBuilder::PARAM_STR))
           );
        }
        if ($dateMax !== null and $dateMax !== '') {
           $qb->andWhere(
               $qb->expr()->lte('date', $qb->createNamedParameter($dateMax, IQueryBuilder::PARAM_STR))
           );
        }
        if ($paymentMode !== null and $paymentMode !== '' and $paymentMode !== 'n') {
           $qb->andWhere(
               $qb->expr()->eq('paymentmode', $qb->createNamedParameter($paymentMode, IQueryBuilder::PARAM_STR))
           );
        }
        if ($category !== null and $category !== '' and intval($category) !== 0) {
           $qb->andWhere(
               $qb->expr()->eq('categoryid', $qb->createNamedParameter(intval($category), IQueryBuilder::PARAM_INT))
           );
        }
        if ($amountMin !== null and is_numeric($amountMin)) {
           $qb->andWhere(
               $qb->expr()->gte('amount', $qb->createNamedParameter(floatval($amountMin), IQueryBuilder::PARAM_STR))
           );
        }
        if ($amountMax !== null and is_numeric($amountMax)) {
           $qb->andWhere(
               $qb->expr()->lte('amount', $qb->createNamedParameter(floatval($amountMax), IQueryBuilder::PARAM_STR))
           );
        }
        $req = $qb->execute();

        while ($row = $req->fetch()){
            array_push($billIds, $row['id']);
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();

        // get bill owers
        $billOwersByBill = [];
        foreach ($billIds as $billId) {
            $billOwers = [];

            $qb->select('memberid', 'm.name', 'm.weight', 'm.activated')
               ->from('cospend_bill_owers', 'bo')
               ->innerJoin('bo', 'cospend_members', 'm', $qb->expr()->eq('bo.memberid', 'm.id'))
               ->where(
                   $qb->expr()->eq('bo.billid', $qb->createNamedParameter($billId, IQueryBuilder::PARAM_INT))
               );
            $req = $qb->execute();
            while ($row = $req->fetch()){
                $dbWeight = floatval($row['weight']);
                $dbName = $row['name'];
                $dbActivated = (intval($row['activated']) === 1);
                $dbOwerId= intval($row['memberid']);
                array_push(
                    $billOwers,
                    [
                        'id' => $dbOwerId,
                        'weight' => $dbWeight,
                        'name' => $dbName,
                        'activated' => $dbActivated
                    ]
                );
            }
            $req->closeCursor();
            $qb = $qb->resetQueryParts();
            $billOwersByBill[$billId] = $billOwers;
        }

        $qb->select('id', 'what', 'date', 'amount', 'payerid', 'repeat', 'paymentmode', 'categoryid', 'lastchanged')
           ->from('cospend_bills', 'b')
           ->where(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
           );
        // take bills that have changed after $lastchanged
        if ($lastchanged !== null and is_numeric($lastchanged)) {
           $qb->andWhere(
               $qb->expr()->gt('lastchanged', $qb->createNamedParameter(intval($lastchanged), IQueryBuilder::PARAM_INT))
           );
        }
        if ($dateMin !== null and $dateMin !== '') {
           $qb->andWhere(
               $qb->expr()->gte('date', $qb->createNamedParameter($dateMin, IQueryBuilder::PARAM_STR))
           );
        }
        if ($dateMax !== null and $dateMax !== '') {
           $qb->andWhere(
               $qb->expr()->lte('date', $qb->createNamedParameter($dateMax, IQueryBuilder::PARAM_STR))
           );
        }
        if ($paymentMode !== null and $paymentMode !== '' and $paymentMode !== 'n') {
           $qb->andWhere(
               $qb->expr()->eq('paymentmode', $qb->createNamedParameter($paymentMode, IQueryBuilder::PARAM_STR))
           );
        }
        if ($category !== null and $category !== '' and intval($category) !== 0) {
           $qb->andWhere(
               $qb->expr()->eq('categoryid', $qb->createNamedParameter(intval($category), IQueryBuilder::PARAM_INT))
           );
        }
        if ($amountMin !== null and is_numeric($amountMin)) {
           $qb->andWhere(
               $qb->expr()->gte('amount', $qb->createNamedParameter(floatval($amountMin), IQueryBuilder::PARAM_STR))
           );
        }
        if ($amountMax !== null and is_numeric($amountMax)) {
           $qb->andWhere(
               $qb->expr()->lte('amount', $qb->createNamedParameter(floatval($amountMax), IQueryBuilder::PARAM_STR))
           );
        }
        $qb->orderBy('date', 'ASC');
        $req = $qb->execute();
        while ($row = $req->fetch()){
            $dbBillId = intval($row['id']);
            $dbAmount = floatval($row['amount']);
            $dbWhat = $row['what'];
            $dbDate = $row['date'];
            $dbRepeat = $row['repeat'];
            $dbPayerId = intval($row['payerid']);
            $dbPaymentMode = $row['paymentmode'];
            $dbCategoryId = intval($row['categoryid']);
            $dbLastchanged = intval($row['lastchanged']);
            array_push(
                $bills,
                [
                    'id' => $dbBillId,
                    'amount' => $dbAmount,
                    'what' => $dbWhat,
                    'date' => $dbDate,
                    'payer_id' => $dbPayerId,
                    'owers' => $billOwersByBill[$row['id']],
                    'repeat' => $dbRepeat,
                    'paymentmode' => $dbPaymentMode,
                    'categoryid' => $dbCategoryId,
                    'lastchanged' => $dbLastchanged
                ]
            );
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();

        return $bills;
    }

    public function getAllBillIds($projectId) {
        $billIds = [];
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('id')
           ->from('cospend_bills', 'b')
           ->where(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
           );
        $req = $qb->execute();

        while ($row = $req->fetch()){
            array_push($billIds, $row['id']);
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();

        return $billIds;
    }

    public function getMembers($projectId, $order=null, $lastchanged=null) {
        $members = [];

        $sqlOrder = 'name';
        if ($order !== null) {
            if ($order === 'lowername') {
                $sqlOrder = 'name';
            }
            else {
                $sqlOrder = $order;
            }
        }

        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('id', 'name', 'weight', 'activated', 'lastchanged')
           ->from('cospend_members', 'm')
           ->where(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
           );
        if ($lastchanged !== null and is_numeric($lastchanged)) {
           $qb->andWhere(
               $qb->expr()->gt('lastchanged', $qb->createNamedParameter($lastchanged, IQueryBuilder::PARAM_INT))
           );
        }
        $qb->orderBy($sqlOrder, 'ASC');
        $req = $qb->execute();

        if ($order === 'lowername') {
            while ($row = $req->fetch()){
                $dbMemberId = intval($row['id']);
                $dbWeight = floatval($row['weight']);
                $dbName = $row['name'];
                $dbActivated = intval($row['activated']);
                $dbLastchanged = intval($row['lastchanged']);
                $av = $this->avatarManager->getGuestAvatar($dbName);
                $color = $av->avatarBackgroundColor($dbName);

                // find index to make sorted insert
                $ii = 0;
                while ($ii < count($members) && strcmp(strtolower($dbName), strtolower($members[$ii]['name'])) > 0) {
                    $ii++;
                }

                array_splice(
                    $members,
                    $ii,
                    0,
                    [[
                        'activated' => ($dbActivated === 1),
                        'name' => $dbName,
                        'id' => $dbMemberId,
                        'weight' => $dbWeight,
                        'color'=>$color,
                        'lastchanged'=>$dbLastchanged
                    ]]
                );
            }
        }
        else {
            while ($row = $req->fetch()){
                $dbMemberId = intval($row['id']);
                $dbWeight = floatval($row['weight']);
                $dbName = $row['name'];
                $dbActivated = intval($row['activated']);
                $dbLastchanged = intval($row['lastchanged']);
                $av = $this->avatarManager->getGuestAvatar($dbName);
                $color = $av->avatarBackgroundColor($dbName);

                array_push(
                    $members,
                    [
                        'activated' => ($dbActivated === 1),
                        'name' => $dbName,
                        'id' => $dbMemberId,
                        'weight' => $dbWeight,
                        'color'=>$color,
                        'lastchanged'=>$dbLastchanged
                    ]
                );
            }
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();
        return $members;
    }

    private function getBalance($projectId) {
        $membersWeight = [];
        $membersBalance = [];

        $members = $this->getMembers($projectId);
        foreach ($members as $member) {
            $memberId = $member['id'];
            $memberWeight = $member['weight'];
            $membersWeight[$memberId] = $memberWeight;
            $membersBalance[$memberId] = 0.0;
        }

        $bills = $this->getBills($projectId);
        foreach ($bills as $bill) {
            $payerId = $bill['payer_id'];
            $amount = $bill['amount'];
            $owers = $bill['owers'];

            $membersBalance[$payerId] += $amount;

            $nbOwerShares = 0.0;
            foreach ($owers as $ower) {
                $owerWeight = $ower['weight'];
                if ($owerWeight === 0.0) {
                    $owerWeight = 1.0;
                }
                $nbOwerShares += $owerWeight;
            }
            foreach ($owers as $ower) {
                $owerWeight = $ower['weight'];
                if ($owerWeight === 0.0) {
                    $owerWeight = 1.0;
                }
                $owerId = $ower['id'];
                $spent = $amount / $nbOwerShares * $owerWeight;
                $membersBalance[$owerId] -= $spent;
            }
        }

        return $membersBalance;
    }

    public function getProjects($userId) {
        $projects = [];
        $projectids = [];

        $qb = $this->dbconnection->getQueryBuilder();

        $qb->select('p.id', 'p.password', 'p.name', 'p.email', 'p.autoexport')
           ->from('cospend_projects', 'p')
           ->where(
               $qb->expr()->eq('userid', $qb->createNamedParameter($userId, IQueryBuilder::PARAM_STR))
           );
        $req = $qb->execute();

        $dbProjectId = null;
        $dbPassword = null;
        while ($row = $req->fetch()){
            $dbProjectId = $row['id'];
            array_push($projectids, $dbProjectId);
            $dbPassword = $row['password'];
            $dbName  = $row['name'];
            $dbEmail = $row['email'];
            $autoexport = $row['autoexport'];
            array_push($projects, [
                'name'=>$dbName,
                'contact_email'=>$dbEmail,
                'id'=>$dbProjectId,
                'autoexport'=>$autoexport,
                'active_members'=>null,
                'members'=>null,
                'balance'=>null,
                'shares'=>[]
            ]);
        }
        $req->closeCursor();

        $qb = $qb->resetQueryParts();

        // shared with user
        $qb->select('p.id', 'p.password', 'p.name', 'p.email', 'p.autoexport')
           ->from('cospend_projects', 'p')
           ->innerJoin('p', 'cospend_shares', 's', $qb->expr()->eq('p.id', 's.projectid'))
           ->where(
               $qb->expr()->eq('s.userid', $qb->createNamedParameter($userId, IQueryBuilder::PARAM_STR))
           )
           ->andWhere(
               $qb->expr()->eq('s.isgroupshare', $qb->createNamedParameter(0, IQueryBuilder::PARAM_INT))
           );
        $req = $qb->execute();

        $dbProjectId = null;
        $dbPassword = null;
        while ($row = $req->fetch()){
            $dbProjectId = $row['id'];
            // avoid putting twice the same project
            // this can happen with a share loop
            if (!in_array($dbProjectId, $projectids)) {
                $dbPassword = $row['password'];
                $dbName = $row['name'];
                $dbEmail= $row['email'];
                $autoexport = $row['autoexport'];
                array_push($projects, [
                    'name'=>$dbName,
                    'contact_email'=>$dbEmail,
                    'id'=>$dbProjectId,
                    'autoexport'=>$autoexport,
                    'active_members'=>null,
                    'members'=>null,
                    'balance'=>null,
                    'shares'=>[]
                ]);
                array_push($projectids, $dbProjectId);
            }
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();

        // shared with one of the groups the user is member of
        $userO = $this->userManager->get($userId);

        // get group with which a project is shared
        $candidateGroupIds = [];
        $qb->select('userid')
           ->from('cospend_shares', 's')
           ->where(
               $qb->expr()->eq('isgroupshare', $qb->createNamedParameter(1, IQueryBuilder::PARAM_INT))
           )
           ->groupBy('userid');
        $req = $qb->execute();
        while ($row = $req->fetch()){
            $groupId = $row['userid'];
            array_push($candidateGroupIds, $groupId);
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();

        // is the user member of these groups?
        foreach ($candidateGroupIds as $candidateGroupId) {
            $group = $this->groupManager->get($candidateGroupId);
            if ($group !== null && $group->inGroup($userO)) {
                // get projects shared with this group
                $qb->select('p.id', 'p.password', 'p.name', 'p.email', 'p.autoexport')
                    ->from('cospend_projects', 'p')
                    ->innerJoin('p', 'cospend_shares', 's', $qb->expr()->eq('p.id', 's.projectid'))
                    ->where(
                        $qb->expr()->eq('s.userid', $qb->createNamedParameter($candidateGroupId, IQueryBuilder::PARAM_STR))
                    )
                    ->andWhere(
                        $qb->expr()->eq('s.isgroupshare', $qb->createNamedParameter(1, IQueryBuilder::PARAM_INT))
                    );
                $req = $qb->execute();

                $dbProjectId = null;
                $dbPassword = null;
                while ($row = $req->fetch()){
                    $dbProjectId = $row['id'];
                    // avoid putting twice the same project
                    // this can happen with a share loop
                    if (!in_array($dbProjectId, $projectids)) {
                        $dbPassword = $row['password'];
                        $dbName = $row['name'];
                        $dbEmail= $row['email'];
                        $autoexport = $row['autoexport'];
                        array_push($projects, [
                            'name'=>$dbName,
                            'contact_email'=>$dbEmail,
                            'id'=>$dbProjectId,
                            'autoexport'=>$autoexport,
                            'active_members'=>null,
                            'members'=>null,
                            'balance'=>null,
                            'shares'=>[]
                        ]);
                        array_push($projectids, $dbProjectId);
                    }
                }
                $req->closeCursor();
                $qb = $qb->resetQueryParts();
            }
        }

        // get values for projects we're gonna return
        for ($i = 0; $i < count($projects); $i++) {
            $dbProjectId = $projects[$i]['id'];
            $members = $this->getMembers($dbProjectId, 'lowername');
            $shares = $this->getUserShares($dbProjectId);
            $groupShares = $this->getGroupShares($dbProjectId);
            $activeMembers = [];
            foreach ($members as $member) {
                if ($member['activated']) {
                    array_push($activeMembers, $member);
                }
            }
            $balance = $this->getBalance($dbProjectId);
            $projects[$i]['active_members'] = $activeMembers;
            $projects[$i]['members'] = $members;
            $projects[$i]['balance'] = $balance;
            $projects[$i]['shares'] = $shares;
            $projects[$i]['group_shares'] = $groupShares;
        }

        // get external projects
        $qb->select('ep.projectid', 'ep.password', 'ep.ncurl')
           ->from('cospend_ext_projects', 'ep')
           ->where(
               $qb->expr()->eq('userid', $qb->createNamedParameter($userId, IQueryBuilder::PARAM_STR))
           );
        $req = $qb->execute();

        while ($row = $req->fetch()){
            $dbProjectId = $row['projectid'];
            $dbPassword = $row['password'];
            $dbNcUrl = $row['ncurl'];
            array_push($projects, [
                'name'=>$dbProjectId.'@'.$dbNcUrl,
                'ncurl'=>$dbNcUrl,
                'id'=>$dbProjectId,
                'password'=>$dbPassword,
                'active_members'=>null,
                'members'=>null,
                'balance'=>null,
                'shares'=>[],
                'external'=>true
            ]);
        }
        $req->closeCursor();

        return $projects;
    }

    private function getUserShares($projectid) {
        $shares = [];

        $userIdToName = [];
        foreach($this->userManager->search('') as $u) {
            $userIdToName[$u->getUID()] = $u->getDisplayName();
        }

        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('projectid', 'userid')
           ->from('cospend_shares', 'sh')
           ->where(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
           )
           ->andWhere(
               $qb->expr()->eq('isgroupshare', $qb->createNamedParameter(0, IQueryBuilder::PARAM_INT))
           );
        $req = $qb->execute();
        while ($row = $req->fetch()){
            $dbuserId = $row['userid'];
            $dbprojectId = $row['projectid'];
            if (array_key_exists($dbuserId, $userIdToName)) {
                array_push($shares, ['userid'=>$dbuserId, 'name'=>$userIdToName[$dbuserId]]);
            }
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();

        return $shares;
    }

    private function getGroupShares($projectid) {
        $shares = [];

        $groupIdToName = [];
        foreach($this->groupManager->search('') as $g) {
            $groupIdToName[$g->getGID()] = $g->getDisplayName();
        }

        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('projectid', 'userid')
           ->from('cospend_shares', 'sh')
           ->where(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
           )
           ->andWhere(
               $qb->expr()->eq('isgroupshare', $qb->createNamedParameter(1, IQueryBuilder::PARAM_INT))
           );
        $req = $qb->execute();
        while ($row = $req->fetch()){
            $dbGroupId = $row['userid'];
            $dbprojectId = $row['projectid'];
            if (array_key_exists($dbGroupId, $groupIdToName)) {
                array_push($shares, ['groupid'=>$dbGroupId, 'name'=>$groupIdToName[$dbGroupId]]);
            }
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();

        return $shares;
    }

    public function deleteMember($projectid, $memberid) {
        $memberToDelete = $this->getMemberById($projectid, $memberid);
        if ($memberToDelete !== null) {
            if ($memberToDelete['activated']) {
                $qb = $this->dbconnection->getQueryBuilder();
                $qb->update('cospend_members');
                $qb->set('activated', $qb->createNamedParameter(0, IQueryBuilder::PARAM_INT));
                $qb->where(
                    $qb->expr()->eq('id', $qb->createNamedParameter($memberid, IQueryBuilder::PARAM_INT))
                )
                ->andWhere(
                    $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
                );
                $req = $qb->execute();
                $qb = $qb->resetQueryParts();
            }
            return 'OK';
        }
        else {
            return ['Not Found'];
        }
    }

    public function getMemberByName($projectId, $name) {
        $member = null;
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('id', 'name', 'weight', 'activated')
           ->from('cospend_members', 'm')
           ->where(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
           )
           ->andWhere(
               $qb->expr()->eq('name', $qb->createNamedParameter($name, IQueryBuilder::PARAM_STR))
           );
        $req = $qb->execute();

        while ($row = $req->fetch()){
            $dbMemberId = intval($row['id']);
            $dbWeight = floatval($row['weight']);
            $dbName = $row['name'];
            $dbActivated= intval($row['activated']);
            $member = [
                    'activated' => ($dbActivated === 1),
                    'name' => $dbName,
                    'id' => $dbMemberId,
                    'weight' => $dbWeight
            ];
            break;
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();
        return $member;
    }

    public function editBill($projectid, $billid, $date, $what, $payer, $payed_for,
                              $amount, $repeat, $paymentmode=null, $categoryid=null) {
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->update('cospend_bills');

        // set last modification timestamp
        $ts = (new \DateTime())->getTimestamp();
        $qb->set('lastchanged', $qb->createNamedParameter($ts, IQueryBuilder::PARAM_INT));

        // first check the bill exists
        if ($this->getBill($projectid, $billid) === null) {
            return ['message'=> ['There is no such bill']];
        }
        // then edit the hell of it
        if ($what === null || $what === '') {
            return ['what'=> ['This field is required.']];
        }
        $qb->set('what', $qb->createNamedParameter($what, IQueryBuilder::PARAM_STR));

        if ($repeat === null || $repeat === '' || strlen($repeat) !== 1) {
            return ['repeat'=> ['Invalid value.']];
        }
        $qb->set('repeat', $qb->createNamedParameter($repeat, IQueryBuilder::PARAM_STR));

        if ($paymentmode !== null && is_string($paymentmode)) {
            $qb->set('paymentmode', $qb->createNamedParameter($paymentmode, IQueryBuilder::PARAM_STR));
        }
        if ($categoryid !== null && is_numeric($categoryid)) {
            $qb->set('categoryid', $qb->createNamedParameter($categoryid, IQueryBuilder::PARAM_INT));
        }
        if ($date !== null && $date !== '') {
            $qb->set('date', $qb->createNamedParameter($date, IQueryBuilder::PARAM_STR));
        }
        if ($amount !== null && $amount !== '' && is_numeric($amount)) {
            $qb->set('amount', $qb->createNamedParameter($amount, IQueryBuilder::PARAM_STR));
        }
        if ($payer !== null && $payer !== '' && is_numeric($payer)) {
            $member = $this->getMemberById($projectid, $payer);
            if ($member === null) {
                return ['payer'=>['Not a valid choice']];
            }
            else {
                $qb->set('payerid', $qb->createNamedParameter($payer, IQueryBuilder::PARAM_INT));
            }
        }

        $owerIds = null;
        // check owers
        if ($payed_for !== null && $payed_for !== '') {
            $owerIds = explode(',', $payed_for);
            if (count($owerIds) === 0) {
                return ['payed_for'=>['Invalid value']];
            }
            else {
                foreach ($owerIds as $owerId) {
                    if (!is_numeric($owerId)) {
                        return ['payed_for'=>['Invalid value']];
                    }
                    if ($this->getMemberById($projectid, $owerId) === null) {
                        return ['payed_for'=>['Not a valid choice']];
                    }
                }
            }
        }

        // do it already !
        $qb->where(
               $qb->expr()->eq('id', $qb->createNamedParameter($billid, IQueryBuilder::PARAM_INT))
           )
           ->andWhere(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
           );
        $req = $qb->execute();
        $qb = $qb->resetQueryParts();

        // edit the bill owers
        if ($owerIds !== null) {
            // delete old bill owers
            $this->deleteBillOwersOfBill($billid);
            // insert bill owers
            foreach ($owerIds as $owerId) {
                $qb->insert('cospend_bill_owers')
                    ->values([
                        'billid' => $qb->createNamedParameter($billid, IQueryBuilder::PARAM_INT),
                        'memberid' => $qb->createNamedParameter($owerId, IQueryBuilder::PARAM_INT)
                    ]);
                $req = $qb->execute();
                $qb = $qb->resetQueryParts();
            }
        }

        return intval($billid);
    }

    /**
     * daily check of repeated bills
     */
    public function cronRepeatBills() {
        $result = [];
        $projects = [];
        $now = new \DateTime();
        // in case cron job wasn't executed during several days,
        // continue trying to repeat bills as long as there was at least one repeated
        $continue = true;
        while ($continue) {
            $continue = false;
            // get bills whith repetition flag
            $qb = $this->dbconnection->getQueryBuilder();
            $qb->select('id', 'projectid', 'what', 'date', 'amount', 'payerid', 'repeat')
            ->from('cospend_bills', 'b')
            ->where(
                $qb->expr()->neq('repeat', $qb->createNamedParameter('n', IQueryBuilder::PARAM_STR))
            );
            $req = $qb->execute();
            $bills = [];
            while ($row = $req->fetch()){
                $id = $row['id'];
                $what = $row['what'];
                $repeat = $row['repeat'];
                $date = $row['date'];
                $projectid = $row['projectid'];
                array_push($bills, [
                    'id' => $id,
                    'what' => $what,
                    'repeat' => $repeat,
                    'date' => $date,
                    'projectid' => $projectid
                ]);
            }
            $req->closeCursor();
            $qb = $qb->resetQueryParts();

            foreach ($bills as $bill) {
                // Use DateTimeImmutable instead of DateTime so that $billDate->add() returns a
                // new instance instead of modifying $billDate
                $billDate = new \DateTimeImmutable($bill['date']);

                $nextDate = null;
                switch($bill['repeat']) {
                case 'd':
                    $nextDate = $billDate->add(new \DateInterval('P1D'));
                    break;

                case 'w';
                    $nextDate = $billDate->add(new \DateInterval('P7D'));
                    break;

                case 'm':
                    if (intval($billDate->format('m')) === 12) {
                        $nextYear = $billDate->format('Y') + 1;
                        $nextMonth = 1;
                    }
                    else {
                        $nextYear = $billDate->format('Y');
                        $nextMonth = $billDate->format('m') + 1;
                    }

                    // same day of month if possible, otherwise at end of month
                    $nextDate = new DateTime();
                    $nextDate->setDate($nextYear, $nextMonth, 1);
                    if ($billDate->format('d') > $nextDate->format('t')) {
                        $nextDate->setDate($nextYear, $nextMonth, $nextDate->format('t'));
                    }
                    else {
                        $nextDate->setDate($nextYear, $nextMonth, $billDate->format('d'));
                    }
                    break;

                case 'y':
                    $nextYear = $billDate->format('Y') + 1;
                    $nextMonth = $billDate->format('m');

                    // same day of month if possible, otherwise at end of month + same month
                    $nextDate = new \DateTime();
                    $nextDate->setDate($billDate->format('Y') + 1, $billDate->format('m'), 1);
                    if ($billDate->format('d') > $nextDate->format('t')) {
                        $nextDate->setDate($nextYear, $nextMonth, $nextDate->format('t'));
                    }
                    else {
                        $nextDate->setDate($nextYear, $nextMonth, $billDate->format('d'));
                    }
                    break;
                }

                // Unknown repeat interval
                if ($nextDate === null) {
                    continue;
                }

                // Repeat if $nextDate is in the past (or today)
                $diff = $now->diff($nextDate);
                if ($nextDate->format('Y-m-d') === $billDate->format('Y-m-d') || $diff->invert) {
                    $this->repeatBill($bill['projectid'], $bill['id'], $nextDate);
                    if (!array_key_exists($bill['projectid'], $projects)) {
                        $projects[$bill['projectid']] = $this->getProjectInfo($bill['projectid']);
                    }
                    array_push($result, [
                        'date_orig' => $bill['date'],
                        'date_repeat' => $nextDate->format('Y-m-d'),
                        'what' => $bill['what'],
                        'project_name' => $projects[$bill['projectid']]['name']
                    ]);
                    $continue = true;
                }
            }
        }
        return $result;
    }

    /**
     * duplicate the bill today and give it the repeat flag
     * remove the repeat flag on original bill
     */
    private function repeatBill($projectid, $billid, $datetime) {
        $bill = $this->getBill($projectid, $billid);

        $owerIds = [];
        foreach ($bill['owers'] as $ower) {
            array_push($owerIds, $ower['id']);
        }
        $owerIdsStr = implode(',', $owerIds);

        $newBillId = $this->addBill($projectid, $datetime->format('Y-m-d'), $bill['what'], $bill['payer_id'],
                       $owerIdsStr, $bill['amount'], $bill['repeat'], $bill['paymentmode'], $bill['categoryid']);

        $billObj = $this->billMapper->find($newBillId);
        $this->activityManager->triggerEvent(
            ActivityManager::COSPEND_OBJECT_BILL, $billObj,
            ActivityManager::SUBJECT_BILL_CREATE,
            []
        );

        // now we can remove repeat flag on original bill
        $this->editBill($projectid, $billid, $bill['date'], $bill['what'], $bill['payer_id'], null, $bill['amount'], 'n');
    }

    public function addUserShare($projectid, $userid, $fromUserId) {
        // check if userId exists
        $userIds = [];
        foreach($this->userManager->search('') as $u) {
            if ($u->getUID() !== $fromUserId) {
                array_push($userIds, $u->getUID());
            }
        }
        if ($userid !== '' and in_array($userid, $userIds)) {
            $qb = $this->dbconnection->getQueryBuilder();
            $projectInfo = $this->getProjectInfo($projectid);
            // check if someone tries to share the project with its owner
            if ($userid !== $projectInfo['userid']) {
                // check if user share exists
                $qb->select('userid', 'projectid')
                    ->from('cospend_shares', 's')
                    ->where(
                        $qb->expr()->eq('isgroupshare', $qb->createNamedParameter(0, IQueryBuilder::PARAM_INT))
                    )
                    ->andWhere(
                        $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
                    )
                    ->andWhere(
                        $qb->expr()->eq('userid', $qb->createNamedParameter($userid, IQueryBuilder::PARAM_STR))
                    );
                $req = $qb->execute();
                $dbuserId = null;
                while ($row = $req->fetch()){
                    $dbuserId = $row['userid'];
                    break;
                }
                $req->closeCursor();
                $qb = $qb->resetQueryParts();

                if ($dbuserId === null) {
                    $qb->insert('cospend_shares')
                        ->values([
                            'projectid' => $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR),
                            'userid' => $qb->createNamedParameter($userid, IQueryBuilder::PARAM_STR),
                            'isgroupshare' => $qb->createNamedParameter(0, IQueryBuilder::PARAM_INT)
                        ]);
                    $req = $qb->execute();
                    $qb = $qb->resetQueryParts();

                    $response = 'OK';

                    // activity
                    $projectObj = $this->projectMapper->find($projectid);
                    $this->activityManager->triggerEvent(
                        ActivityManager::COSPEND_OBJECT_PROJECT, $projectObj,
                        ActivityManager::SUBJECT_PROJECT_SHARE,
                        ['who'=>$userid, 'type'=>'u']
                    );

                    // SEND NOTIFICATION
                    $manager = \OC::$server->getNotificationManager();
                    $notification = $manager->createNotification();

                    $acceptAction = $notification->createAction();
                    $acceptAction->setLabel('accept')
                        ->setLink('/apps/cospend', 'GET');

                    $declineAction = $notification->createAction();
                    $declineAction->setLabel('decline')
                        ->setLink('/apps/cospend', 'GET');

                    $notification->setApp('cospend')
                        ->setUser($userid)
                        ->setDateTime(new \DateTime())
                        ->setObject('addusershare', $projectid)
                        ->setSubject('add_user_share', [$fromUserId, $projectInfo['name']])
                        ->addAction($acceptAction)
                        ->addAction($declineAction)
                        ;

                    $manager->notify($notification);
                }
                else {
                    $response = ['message'=>'Already shared with this user'];
                }
            }
            else {
                $response = ['message'=>'Impossible to share the project with its owner'];
            }
        }
        else {
            $response = ['message'=>'No such user'];
        }

        return $response;
    }

    public function deleteUserShare($projectid, $userid, $fromUserId) {
        // check if user share exists
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('userid', 'projectid')
            ->from('cospend_shares', 's')
            ->where(
                $qb->expr()->eq('isgroupshare', $qb->createNamedParameter(0, IQueryBuilder::PARAM_INT))
            )
            ->andWhere(
                $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
            )
            ->andWhere(
                $qb->expr()->eq('userid', $qb->createNamedParameter($userid, IQueryBuilder::PARAM_STR))
            );
        $req = $qb->execute();
        $dbuserId = null;
        while ($row = $req->fetch()){
            $dbuserId = $row['userid'];
            break;
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();

        if ($dbuserId !== null) {
            // delete
            $qb->delete('cospend_shares')
                ->where(
                    $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
                )
                ->andWhere(
                    $qb->expr()->eq('userid', $qb->createNamedParameter($userid, IQueryBuilder::PARAM_STR))
                )
                ->andWhere(
                    $qb->expr()->eq('isgroupshare', $qb->createNamedParameter(0, IQueryBuilder::PARAM_INT))
                );
            $req = $qb->execute();
            $qb = $qb->resetQueryParts();

            $response = 'OK';

            // activity
            $projectObj = $this->projectMapper->find($projectid);
            $this->activityManager->triggerEvent(
                ActivityManager::COSPEND_OBJECT_PROJECT, $projectObj,
                ActivityManager::SUBJECT_PROJECT_UNSHARE,
                ['who'=>$userid, 'type'=>'u']
            );

            // SEND NOTIFICATION
            $projectInfo = $this->getProjectInfo($projectid);

            $manager = \OC::$server->getNotificationManager();
            $notification = $manager->createNotification();

            $acceptAction = $notification->createAction();
            $acceptAction->setLabel('accept')
                ->setLink('/apps/cospend', 'GET');

            $declineAction = $notification->createAction();
            $declineAction->setLabel('decline')
                ->setLink('/apps/cospend', 'GET');

            $notification->setApp('cospend')
                ->setUser($userid)
                ->setDateTime(new \DateTime())
                ->setObject('deleteusershare', $projectid)
                ->setSubject('delete_user_share', [$fromUserId, $projectInfo['name']])
                ->addAction($acceptAction)
                ->addAction($declineAction)
                ;

            $manager->notify($notification);
        }
        else {
            $response = ['message'=>'No such share'];
        }

        return $response;
    }

    public function addGroupShare($projectid, $groupid, $fromUserId) {
        // check if groupId exists
        $groupIds = [];
        foreach($this->groupManager->search('') as $g) {
            array_push($groupIds, $g->getGID());
        }
        if ($groupid !== '' and in_array($groupid, $groupIds)) {
            $qb = $this->dbconnection->getQueryBuilder();
            $projectInfo = $this->getProjectInfo($projectid);
            // check if user share exists
            $qb->select('userid', 'projectid')
                ->from('cospend_shares', 's')
                ->where(
                    $qb->expr()->eq('isgroupshare', $qb->createNamedParameter(1, IQueryBuilder::PARAM_INT))
                )
                ->andWhere(
                    $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
                )
                ->andWhere(
                    $qb->expr()->eq('userid', $qb->createNamedParameter($groupid, IQueryBuilder::PARAM_STR))
                );
            $req = $qb->execute();
            $dbGroupId = null;
            while ($row = $req->fetch()){
                $dbGroupId = $row['userid'];
                break;
            }
            $req->closeCursor();
            $qb = $qb->resetQueryParts();

            if ($dbGroupId === null) {
                $qb->insert('cospend_shares')
                    ->values([
                        'projectid' => $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR),
                        'userid' => $qb->createNamedParameter($groupid, IQueryBuilder::PARAM_STR),
                        'isgroupshare' => $qb->createNamedParameter(1, IQueryBuilder::PARAM_INT)
                    ]);
                $req = $qb->execute();
                $qb = $qb->resetQueryParts();

                $response = 'OK';

                // activity
                $projectObj = $this->projectMapper->find($projectid);
                $this->activityManager->triggerEvent(
                    ActivityManager::COSPEND_OBJECT_PROJECT, $projectObj,
                    ActivityManager::SUBJECT_PROJECT_SHARE,
                    ['who'=>$groupid, 'type'=>'g']
                );

                //// SEND NOTIFICATION
                //$manager = \OC::$server->getNotificationManager();
                //$notification = $manager->createNotification();

                //$acceptAction = $notification->createAction();
                //$acceptAction->setLabel('accept')
                //    ->setLink('/apps/cospend', 'GET');

                //$declineAction = $notification->createAction();
                //$declineAction->setLabel('decline')
                //    ->setLink('/apps/cospend', 'GET');

                //$notification->setApp('cospend')
                //    ->setUser($userid)
                //    ->setDateTime(new \DateTime())
                //    ->setObject('addusershare', $projectid)
                //    ->setSubject('add_user_share', [$fromUserId, $projectInfo['name']])
                //    ->addAction($acceptAction)
                //    ->addAction($declineAction)
                //    ;

                //$manager->notify($notification);
            }
            else {
                $response = ['message'=>'Already shared with this group'];
            }
        }
        else {
            $response = ['message'=>'No such group'];
        }

        return $response;
    }

    public function deleteGroupShare($projectid, $groupid, $fromUserId) {
        // check if group share exists
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('userid', 'projectid')
            ->from('cospend_shares', 's')
            ->where(
                $qb->expr()->eq('isgroupshare', $qb->createNamedParameter(1, IQueryBuilder::PARAM_INT))
            )
            ->andWhere(
                $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
            )
            ->andWhere(
                $qb->expr()->eq('userid', $qb->createNamedParameter($groupid, IQueryBuilder::PARAM_STR))
            );
        $req = $qb->execute();
        $dbGroupId = null;
        while ($row = $req->fetch()){
            $dbGroupId = $row['userid'];
            break;
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();

        if ($dbGroupId !== null) {
            // delete
            $qb->delete('cospend_shares')
                ->where(
                    $qb->expr()->eq('projectid', $qb->createNamedParameter($projectid, IQueryBuilder::PARAM_STR))
                )
                ->andWhere(
                    $qb->expr()->eq('userid', $qb->createNamedParameter($groupid, IQueryBuilder::PARAM_STR))
                )
                ->andWhere(
                    $qb->expr()->eq('isgroupshare', $qb->createNamedParameter(1, IQueryBuilder::PARAM_INT))
                );
            $req = $qb->execute();
            $qb = $qb->resetQueryParts();

            $response = 'OK';

            // activity
            $projectObj = $this->projectMapper->find($projectid);
            $this->activityManager->triggerEvent(
                ActivityManager::COSPEND_OBJECT_PROJECT, $projectObj,
                ActivityManager::SUBJECT_PROJECT_UNSHARE,
                ['who'=>$groupid, 'type'=>'g']
            );

            //// SEND NOTIFICATION
            //$projectInfo = $this->getProjectInfo($projectid);

            //$manager = \OC::$server->getNotificationManager();
            //$notification = $manager->createNotification();

            //$acceptAction = $notification->createAction();
            //$acceptAction->setLabel('accept')
            //    ->setLink('/apps/cospend', 'GET');

            //$declineAction = $notification->createAction();
            //$declineAction->setLabel('decline')
            //    ->setLink('/apps/cospend', 'GET');

            //$notification->setApp('cospend')
            //    ->setUser($userid)
            //    ->setDateTime(new \DateTime())
            //    ->setObject('deleteusershare', $projectid)
            //    ->setSubject('delete_user_share', [$this->userId, $projectInfo['name']])
            //    ->addAction($acceptAction)
            //    ->addAction($declineAction)
            //    ;

            //$manager->notify($notification);
        }
        else {
            $response = ['message'=>'No such share'];
        }

        return $response;
    }

    public function exportCsvSettlement($projectid, $userId) {
        // create Cospend directory if needed
        $userFolder = \OC::$server->getUserFolder($userId);
        if (!$userFolder->nodeExists('/Cospend')) {
            $userFolder->newFolder('Cospend');
        }
        if ($userFolder->nodeExists('/Cospend')) {
            $folder = $userFolder->get('/Cospend');
            if ($folder->getType() !== \OCP\Files\FileInfo::TYPE_FOLDER) {
                $response = ['message'=>'/Cospend is not a folder'];
                return $response;
            }
            else if (!$folder->isCreatable()) {
                $response = ['message'=>'/Cospend is not writeable'];
                return $response;
            }
        }
        else {
            $response = ['message'=>'Impossible to create /Cospend'];
            return $response;
        }

        // create file
        if ($folder->nodeExists($projectid.'-settlement.csv')) {
            $folder->get($projectid.'-settlement.csv')->delete();
        }
        $file = $folder->newFile($projectid.'-settlement.csv');
        $handler = $file->fopen('w');
        fwrite($handler, $this->trans->t('Who pays?').','. $this->trans->t('To whom?').','. $this->trans->t('How much?')."\n");
        $transactions = $this->getProjectSettlement($projectid);

        $members = $this->getMembers($projectid);
        $memberIdToName = [];
        foreach ($members as $member) {
            $memberIdToName[$member['id']] = $member['name'];
        }

        foreach ($transactions as $transaction) {
            fwrite($handler, '"'.$memberIdToName[$transaction['from']].'","'.$memberIdToName[$transaction['to']].'",'.floatval($transaction['amount'])."\n");
        }

        fclose($handler);
        $file->touch();
        $response = ['path'=>'/Cospend/'.$projectid.'-settlement.csv'];
        return $response;
    }

    public function exportCsvStatistics($projectid, $userId, $dateMin=null, $dateMax=null, $paymentMode=null, $category=null,
                                        $amountMin=null, $amountMax=null) {
        // create Cospend directory if needed
        $userFolder = \OC::$server->getUserFolder($userId);
        if (!$userFolder->nodeExists('/Cospend')) {
            $userFolder->newFolder('Cospend');
        }
        if ($userFolder->nodeExists('/Cospend')) {
            $folder = $userFolder->get('/Cospend');
            if ($folder->getType() !== \OCP\Files\FileInfo::TYPE_FOLDER) {
                $response = ['message'=>'/Cospend is not a folder'];
                return $response;
            }
            else if (!$folder->isCreatable()) {
                $response = ['message'=>'/Cospend is not writeable'];
                return $response;
            }
        }
        else {
            $response = ['message'=>'Impossible to create /Cospend'];
            return $response;
        }

        // create file
        if ($folder->nodeExists($projectid.'-stats.csv')) {
            $folder->get($projectid.'-stats.csv')->delete();
        }
        $file = $folder->newFile($projectid.'-stats.csv');
        $handler = $file->fopen('w');
        fwrite($handler, $this->trans->t('Member name').','. $this->trans->t('Paid').','. $this->trans->t('Spent').','. $this->trans->t('Balance')."\n");
        $stats = $this->getProjectStatistics($projectid, 'lowername', $dateMin, $dateMax, $paymentMode,
                                                    $category, $amountMin, $amountMax);
        if (!is_array($stats)) {
        }

        foreach ($stats as $stat) {
            fwrite($handler, '"'.$stat['member']['name'].'",'.floatval($stat['paid']).','.floatval($stat['spent']).','.floatval($stat['balance'])."\n");
        }

        fclose($handler);
        $file->touch();
        $response = ['path'=>'/Cospend/'.$projectid.'-stats.csv'];
        return $response;
    }

    public function exportCsvProject($projectid, $name, $userId) {
        // create Cospend directory if needed
        $userFolder = \OC::$server->getUserFolder($userId);
        if (!$userFolder->nodeExists('/Cospend')) {
            $userFolder->newFolder('Cospend');
        }
        if ($userFolder->nodeExists('/Cospend')) {
            $folder = $userFolder->get('/Cospend');
            if ($folder->getType() !== \OCP\Files\FileInfo::TYPE_FOLDER) {
                $response = ['message'=>'/Cospend is not a folder'];
                return $response;
            }
            else if (!$folder->isCreatable()) {
                $response = ['message'=>'/Cospend is not writeable'];
                return $response;
            }
        }
        else {
            $response = ['message'=>'Impossible to create /Cospend'];
            return $response;
        }

        // create file
        $filename = $projectid.'.csv';
        if ($name !== null) {
            $filename = $name;
        }
        if ($folder->nodeExists($filename)) {
            $folder->get($filename)->delete();
        }
        $file = $folder->newFile($filename);
        $handler = $file->fopen('w');
        fwrite($handler, "what,amount,date,payer_name,payer_weight,owers,repeat,categoryid,paymentmode\n");
        $members = $this->getMembers($projectid);
        $memberIdToName = [];
        $memberIdToWeight = [];
        foreach ($members as $member) {
            $memberIdToName[$member['id']] = $member['name'];
            $memberIdToWeight[$member['id']] = $member['weight'];
            fwrite($handler, 'deleteMeIfYouWant,1,1970-01-01,"'.$member['name'].'",'.floatval($member['weight']).',"'.
                             $member['name'].'",n,,'."\n");;
        }
        $bills = $this->getBills($projectid);
        foreach ($bills as $bill) {
            $owerNames = [];
            foreach ($bill['owers'] as $ower) {
                array_push($owerNames, $ower['name']);
            }
            $owersTxt = implode(', ', $owerNames);

            $payer_id = $bill['payer_id'];
            $payer_name = $memberIdToName[$payer_id];
            $payer_weight = $memberIdToWeight[$payer_id];
            fwrite($handler, '"'.$bill['what'].'",'.floatval($bill['amount']).','.$bill['date'].',"'.$payer_name.'",'.
                             floatval($payer_weight).',"'.$owersTxt.'",'.$bill['repeat'].','.
                             $bill['categoryid'].','.$bill['paymentmode']."\n");
        }

        fclose($handler);
        $file->touch();
        $response = ['path'=>'/Cospend/'.$filename];
        return $response;
    }

    public function importCsvProject($path, $userId) {
        $cleanPath = str_replace(array('../', '..\\'), '',  $path);
        $userFolder = \OC::$server->getUserFolder($userId);
        if ($userFolder->nodeExists($cleanPath)) {
            $file = $userFolder->get($cleanPath);
            if ($file->getType() === \OCP\Files\FileInfo::TYPE_FILE) {
                if (($handle = $file->fopen('r')) !== false) {
                    $columns = [];
                    $membersWeight = [];
                    $bills = [];
                    $row = 0;
                    while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                        // first line : get column order
                        if ($row === 0) {
                            $nbCol = count($data);
                            if ($nbCol < 6) {
                                fclose($handle);
                                $response = ['message'=>'Malformed CSV, bad column number'];
                                return $response;
                            }
                            else {
                                for ($c=0; $c < $nbCol; $c++) {
                                    $columns[$data[$c]] = $c;
                                }
                            }
                            if (!array_key_exists('what', $columns) or
                                !array_key_exists('amount', $columns) or
                                !array_key_exists('date', $columns) or
                                !array_key_exists('payer_name', $columns) or
                                !array_key_exists('payer_weight', $columns) or
                                !array_key_exists('owers', $columns)
                            ) {
                                fclose($handle);
                                $response = ['message'=>'Malformed CSV, bad column names'];
                                return $response;
                            }
                        }
                        // normal line : bill
                        else {
                            $what = $data[$columns['what']];
                            $amount = $data[$columns['amount']];
                            $date = $data[$columns['date']];
                            $payer_name = $data[$columns['payer_name']];
                            $payer_weight = $data[$columns['payer_weight']];
                            $owers = $data[$columns['owers']];
                            $repeat = array_key_exists('repeat', $columns) ? $data[$columns['repeat']] : 'n';
                            $categoryid = array_key_exists('categoryid', $columns) ? intval($data[$columns['categoryid']]) : null;
                            $paymentmode = array_key_exists('paymentmode', $columns) ? $data[$columns['paymentmode']] : null;

                            // manage members
                            if (is_numeric($payer_weight)) {
                                $membersWeight[$payer_name] = floatval($payer_weight);
                            }
                            else {
                                fclose($handle);
                                $response = ['message'=>'Malformed CSV, bad payer weight on line '.$row];
                                return $response;
                            }
                            if (strlen($owers) === 0) {
                                fclose($handle);
                                $response = ['message'=>'Malformed CSV, bad owers on line '.$row];
                                return $response;
                            }
                            $owersArray = explode(', ', $owers);
                            foreach ($owersArray as $ower) {
                                if (strlen($ower) === 0) {
                                    fclose($handle);
                                    $response = ['message'=>'Malformed CSV, bad owers on line '.$row];
                                    return $response;
                                }
                                if (!array_key_exists($ower, $membersWeight)) {
                                    $membersWeight[$ower] = 1.0;
                                }
                            }
                            if (!is_numeric($amount)) {
                                fclose($handle);
                                $response = ['message'=>'Malformed CSV, bad amount on line '.$row];
                                return $response;
                            }
                            array_push($bills,
                                [
                                    'what'=>$what,
                                    'date'=>$date,
                                    'amount'=>$amount,
                                    'payer_name'=>$payer_name,
                                    'owers'=>$owersArray,
                                    'paymentmode'=>$paymentmode,
                                    'categoryid'=>$categoryid
                                ]
                            );
                        }
                        $row++;
                    }
                    fclose($handle);

                    $memberNameToId = [];

                    // add project
                    $user = $this->userManager->get($userId);
                    $userEmail = $user->getEMailAddress();
                    $projectid = str_replace('.csv', '', $file->getName());
                    $projectName = $projectid;
                    $projResult = $this->createProject($projectName, $projectid, '', $userEmail, $userId);
                    if (!is_string($projResult)) {
                        $response = ['message'=>'Error in project creation '.$projResult['message']];
                        return $response;
                    }
                    // add members
                    foreach ($membersWeight as $memberName => $weight) {
                        $memberId =  $this->addMember($projectid, $memberName, $weight);
                        if (!is_numeric($memberId)) {
                            $this->deleteProject($projectid);
                            $response = ['message'=>'Error when adding member '.$memberName];
                            return $response;
                        }
                        $memberNameToId[$memberName] = $memberId;
                    }
                    // add bills
                    foreach ($bills as $bill) {
                        $payerId = $memberNameToId[$bill['payer_name']];
                        $owerIds = [];
                        foreach ($bill['owers'] as $owerName) {
                            array_push($owerIds, $memberNameToId[$owerName]);
                        }
                        $owerIdsStr = implode(',', $owerIds);
                        $addBillResult = $this->addBill($projectid, $bill['date'], $bill['what'], $payerId, $owerIdsStr, $bill['amount'], 'n', $bill['paymentmode'], $bill['categoryid']);
                        if (!is_numeric($addBillResult)) {
                            $this->deleteProject($projectid);
                            $response = ['message'=>'Error when adding bill '.$bill['what']];
                            return $response;
                        }
                    }
                    $response = $projectid;
                }
                else {
                    $response = ['message'=>'Access denied'];
                }
            }
            else {
                $response = ['message'=>'Access denied'];
            }
        }
        else {
            $response = ['message'=>'Access denied'];
        }

        return $response;
    }

    /**
     * auto export
     * triggered by NC cron job
     *
     * export projects
     */
    public function cronAutoExport() {
        date_default_timezone_set('UTC');
        // last day
        $now = new \DateTime();
        $y = $now->format('Y');
        $m = $now->format('m');
        $d = $now->format('d');
        $timestamp = $now->getTimestamp();

        // get begining of today
        $dateMaxDay = new \DateTime($y.'-'.$m.'-'.$d);
        $maxDayTimestamp = $dateMaxDay->getTimestamp();
        $minDayTimestamp = $maxDayTimestamp - 24*60*60;

        $dateMaxDay->modify('-1 day');
        $dailySuffix = '_'.$this->trans->t('daily').'_'.$dateMaxDay->format('Y-m-d');

        // last week
        $now = new \DateTime();
        while (intval($now->format('N')) !== 1) {
            $now->modify('-1 day');
        }
        $y = $now->format('Y');
        $m = $now->format('m');
        $d = $now->format('d');
        $dateWeekMax = new \DateTime($y.'-'.$m.'-'.$d);
        $maxWeekTimestamp = $dateWeekMax->getTimestamp();
        $minWeekTimestamp = $maxWeekTimestamp - 7*24*60*60;
        $dateWeekMin = new \DateTime($y.'-'.$m.'-'.$d);
        $dateWeekMin->modify('-7 day');
        $weeklySuffix = '_'.$this->trans->t('weekly').'_'.$dateWeekMin->format('Y-m-d');

        // last month
        $now = new \DateTime();
        while (intval($now->format('d')) !== 1) {
            $now->modify('-1 day');
        }
        $y = $now->format('Y');
        $m = $now->format('m');
        $d = $now->format('d');
        $dateMonthMax = new \DateTime($y.'-'.$m.'-'.$d);
        $maxMonthTimestamp = $dateMonthMax->getTimestamp();
        $now->modify('-1 day');
        while (intval($now->format('d')) !== 1) {
            $now->modify('-1 day');
        }
        $y = intval($now->format('Y'));
        $m = intval($now->format('m'));
        $d = intval($now->format('d'));
        $dateMonthMin = new \DateTime($y.'-'.$m.'-'.$d);
        $minMonthTimestamp = $dateMonthMin->getTimestamp();
        $monthlySuffix = '_'.$this->trans->t('monthly').'_'.$dateMonthMin->format('Y-m');

        $weekFilterArray = array();
        $weekFilterArray['tsmin'] = $minWeekTimestamp;
        $weekFilterArray['tsmax'] = $maxWeekTimestamp;
        $dayFilterArray = array();
        $dayFilterArray['tsmin'] = $minDayTimestamp;
        $dayFilterArray['tsmax'] = $maxDayTimestamp;
        $monthFilterArray = array();
        $monthFilterArray['tsmin'] = $minMonthTimestamp;
        $monthFilterArray['tsmax'] = $maxMonthTimestamp;

        $qb = $this->dbconnection->getQueryBuilder();

        foreach ($this->userManager->search('') as $u) {
            $uid = $u->getUID();

            $qb->select('p.id', 'p.name', 'p.autoexport')
            ->from('cospend_projects', 'p')
            ->where(
                $qb->expr()->eq('userid', $qb->createNamedParameter($uid, IQueryBuilder::PARAM_STR))
            )
            ->andWhere(
                $qb->expr()->neq('p.autoexport', $qb->createNamedParameter('n', IQueryBuilder::PARAM_STR))
            );
            $req = $qb->execute();

            $dbProjectId = null;
            $dbPassword = null;
            while ($row = $req->fetch()) {
                $dbProjectId = $row['id'];
                $dbName  = $row['name'];
                $autoexport = $row['autoexport'];

                $suffix = $dailySuffix;
                if ($autoexport === 'w') {
                    $suffix = $weeklySuffix;
                }
                else if ($autoexport === 'm') {
                    $suffix = $monthlySuffix;
                }
                // check if file already exists
                $exportName = $dbProjectId.$suffix.'.csv';

                $userFolder = \OC::$server->getUserFolder($uid);
                if (! $userFolder->nodeExists('/Cospend/'.$exportName)) {
                    $this->exportCsvProject($dbProjectId, $exportName, $uid);
                }
            }
            $req->closeCursor();
            $qb = $qb->resetQueryParts();
        }
    }

}
